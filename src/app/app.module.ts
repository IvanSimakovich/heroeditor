import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { FormsModule }   from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { HeroDetailsComponent } from './home/hero-details/hero-details.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeroDetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
